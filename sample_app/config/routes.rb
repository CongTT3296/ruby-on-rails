Rails.application.routes.draw do
  resources :microposts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    #root "application#hello"
    #root :to => "static_pages#contact"
    root "microposts#index"
	get "static_pages/home"
	get "static_pages/help"
	get "static_pages/contact"
end
